package com.example.transaction.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
    private String invoiceNo;

    private String requestDate;

    private String lastTransactionDate;

    private String sku;

    private String amount;

    private String paymentLink;

    private String status;
}
