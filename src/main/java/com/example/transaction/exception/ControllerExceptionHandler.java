package com.example.transaction.exception;

import com.example.transaction.dto.response.Response;
import com.google.zxing.WriterException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({
            RuntimeException.class,
            IOException.class,
            WriterException.class
    })
    public ResponseEntity<Response> handleException(Exception ex) {
        return new ResponseEntity<>(
                Response
                        .builder()
                        .status(400)
                        .message(ex.getMessage())
                        .build()
                , HttpStatus.OK);
    }
}
