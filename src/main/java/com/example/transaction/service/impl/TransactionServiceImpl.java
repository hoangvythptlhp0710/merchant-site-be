package com.example.transaction.service.impl;

import com.example.transaction.dto.response.Response;
import com.example.transaction.entity.Payment;
import com.example.transaction.repository.PaymentRepository;
import com.example.transaction.service.TransactionService;
import com.example.transaction.util.QRCodeGenerator;
import com.google.zxing.WriterException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Date;

@Service
@RequiredArgsConstructor
@Log4j2
public class TransactionServiceImpl implements TransactionService {
    private final PaymentRepository paymentRepository;

    @Value("${log.folder.path}")
    private String LOG_FOLDER_PATH;

    @Override
    public ResponseEntity<Response> getPayment(String invoiceNo) throws IOException {
        writeLog("Trying to get the payment for invoiceNo = " + invoiceNo, false);

        Payment payment = paymentRepository.getPayment(invoiceNo);
        writeLog("Payment for invoice no: " + invoiceNo + " = " + payment, false);

        Response response;

        if (payment == null) {
            response = Response.builder()
                    .status(404)
                    .message("Not Found!")
                    .build();
        } else {
            byte[] image = new byte[0];
            try {
                // Generate by payment link and Return Qr Code in Byte Array
                image = QRCodeGenerator.generateQRCodeImage(payment.getPaymentLink(), 250, 250);
                writeLog("Generate QR Code Image Successfully!", false);
            } catch (WriterException | IOException e) {
                writeLog("Generate QR Code Image Failed!", true);
                e.printStackTrace();
            }
            // Convert Byte Array into Base64 Encode String
            String qrcode = Base64.getEncoder().encodeToString(image);
            writeLog("Convert Byte Array into Base64 Encode String Successfully!", false);

            // set payment link with qrcode
            payment.setPaymentLink(qrcode);


             response = Response.builder()
                    .status(200)
                    .message("Successfully!")
                    .data(payment)
                    .build();
        }



        writeLog("Returning response: " + response, false);

        return ResponseEntity.ok(response);
    }


    private void writeLog(String message, boolean error) throws IOException {
        String fileName = generateFileName();
        String logFolderPath = LOG_FOLDER_PATH;

        File folder = new File(logFolderPath);

        // Create the log folder if it doesn't exist
        if (!folder.exists()) {
            folder.mkdirs();
        }

        File logFile = new File(logFolderPath, fileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true))) {
            // Create a timestamp for the log entry
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = dateFormat.format(new Date());

            // Create the log entry with timestamp and message
            String logEntry;
            if (error) {
                log.error(message);
                logEntry = "[" + timestamp + "] ERROR : " + message;
            } else {
                log.info(message);
                logEntry = "[" + timestamp + "] INFO : " + message;
            }

            // Write the log entry to the file
            writer.write(logEntry);
            writer.newLine(); // Add a newline for each log entry
        } catch (IOException e) {
            // Handle any exceptions that occur during file write
            System.err.println("Error writing to the log file: " + e.getMessage());
        }
    }

    private String generateFileName() {
        // generate file name with format dd_MM_yyyy.log
        LocalDate date = LocalDate.now();
        return date.getDayOfMonth() + "_" + date.getMonthValue() + "_" + date.getYear() + ".log";
    }
}
