package com.example.transaction.service;

import org.springframework.http.ResponseEntity;
import com.example.transaction.dto.response.Response;

import java.io.IOException;

public interface TransactionService {
    ResponseEntity<Response> getPayment(String invoiceNo) throws IOException;
}
