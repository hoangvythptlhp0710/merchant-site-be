package com.example.transaction.repository;

import com.example.transaction.entity.Payment;
import com.example.transaction.mapper.PaymentMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
@RequiredArgsConstructor
public class PaymentRepository {
    private final PaymentMapper paymentMapper;

    public Payment getPayment(String invoiceNo) {
        log.info("Get Payment in PaymentRepository");
        return paymentMapper.getPayment(invoiceNo);
    }
}
