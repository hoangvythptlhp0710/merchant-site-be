package com.example.transaction.mapper;

import com.example.transaction.entity.Payment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaymentMapper {
    Payment getPayment(String invoiceNo);
}
