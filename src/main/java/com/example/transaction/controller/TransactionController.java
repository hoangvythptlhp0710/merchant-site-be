package com.example.transaction.controller;

import com.example.transaction.dto.response.Response;
import com.example.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@Log4j2
public class TransactionController {
    private final TransactionService transactionService;

    @GetMapping("/payment/{invoiceNo}")
    public ResponseEntity<Response> getPayment(@PathVariable String invoiceNo) throws IOException {
        log.info("Get Payment in TransactionController");
        return transactionService.getPayment(invoiceNo);
    }
}
