FROM openjdk:19-jdk-alpine
WORKDIR /app
COPY target/transaction-0.0.1-SNAPSHOT.jar transaction-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","transaction-0.0.1-SNAPSHOT.jar"]
